% gps_estimate() - Estimated position and time in the GPS problem using the
% Gradient-Descent and Gauss-Newton alorithms
%
% Input:
%   Sl      - Coordinates of the four satellites. One on each column Dimension[3,4]
%   y       - Vector of measures or pseudorange
%
% Optional input:
%   step      - Step size. Default: [0.1]
%   X0        - Vector f initail conditions. Dimension [4,1]. Default: [100 100 100 5]
%   tol       - Tolerance or minimun value of the loss function to
%               determine the convergence. Default: [1e-20]
%   maxiter   - Maximum number of iterations. Default: [500000]
%   method    - Method to use . Graddient-Descent or Gauss-Newton
%               {'graddescend','gauss'}. Default: 'graddescend'
% 
%
% Outputs:
%   Xresult    - The three coordinates of the position and the time bias estimated 
%   Xiter      - Value of X for each iteration
%               coefficients and the quantized signal (input)
%   lossval    - Loss function value for each iteration
%   
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function [Xresult,Xiter,lossval] = gps_estimate(Sl,y,varargin)

try
    options = varargin;
    if ~isempty( varargin ),
        for i = 1:2:numel(options)
            g.(options{i}) = options{i+1};
        end
    else g= []; 
    end;
catch
    disp('gps_estimate() error: calling convention {''key'', value, ... } error'); return;
end;
try g.step;      catch, g.step    = 0.1;                end;
try g.X0;        catch, g.X0      = [100 100 100 5]';   end;
try g.tol;       catch, g.tol     = 1e-20;              end;
try g.maxiter;   catch, g.maxiter = 500000;             end;
try g.method;    catch, g.method  = 'graddescend';      end;

% Initaializing variables 
i = 1; lossvaltmp = Inf; lossval = [];
X0 = g.X0;
Xiter = nan(4,1);

% Grad Descend
switch g.method
    case 'graddescend'
        while lossvaltmp >=g.tol && i <= g.maxiter
            if i==1,X = X0; end
            grad  = compgradient(Sl,X(1:3));
            h     = h_hat(Sl,X);
            
            %Gradient descend
            Xpred = X + g.step*grad'*(y-h);
            
            % Computing convergence criteria
            hpred = h_hat(Sl,Xpred);
            lossvaltmp = 0.5*norm(y-hpred)^2;
            X = Xpred;
            
            % Saving iter evolution
            Xiter(:,i) = X;
            lossval(i) = lossvaltmp;

            i = i + 1; % Counter increment
        end
    case 'gauss'
        while lossvaltmp >=g.tol && i <= g.maxiter
            if i==1,X = X0; end
            grad  = compgradient(Sl,X(1:3));
            h     = h_hat(Sl,X);
            
            % Gauss-Newton
            Xpred = X + g.step*grad\(y-h);
            
            hpred = h_hat(Sl,Xpred);
            lossvaltmp = 0.5*norm(y-hpred)^2;
            X = Xpred;
            
            % Saving iter evolution
            Xiter(:,i) = X;
            lossval(i) = lossvaltmp;
            
            i = i + 1; % Counter increment
        end
end
Xresult = Xpred;
end
%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
%% Compute gradient
function grad = compgradient(Sl,Xpos)
grad = ones(4);
for igrad = 1:4
    grad(igrad,1:3) = (Xpos-Sl(:,igrad))/norm(Xpos-Sl(:,igrad));
end
end

%% Compute h
function h = h_hat(Sl,X)
h = nan(4,1);
for ih = 1:4
    h(ih) = norm(X(1:3)-Sl(:,ih))+ X(4);
end
end