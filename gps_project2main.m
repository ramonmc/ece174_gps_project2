%% Script to generate figures and results for Project 2: GPS Algorithm
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)

%% Initializing values
ER = 6.369999998847030e+06;  % in meters
file2savefigs = pwd;
flagprint = 0;

%% Simulating data
[y, SrealOut, Slsim, bsimOut] = gps_simulation;

 %% Estimating data
 % Initial conditions
 S0 = [.9331 .25 .258819]';
 b0 = 0;
 X0 = [S0;b0];
 
% Estimating position using Gradient-Descent Method.
% Example on how to run the gps_estimate with Gradient-Descent Method.
[Xresult_gd,Xiter_gd,lossval_gd] = gps_estimate(Slsim,y,'X0',X0,'step',0.1,'method','graddescend');

 % Estimating position using Gauss-Newton Method
 % Example on how to run the gps_estimate with Gauss-Newton Method.
[Xresult_gn,Xiter_gn,lossval_gn] = gps_estimate(Slsim,y,'X0',X0,'step',1,'method','gauss');

%%
% Generating vector of step sizes
stepgd = [0.3:(0.01-0.3)/7: 0.01];
stepgn = [2:(0.7-2)/7:0.7];

% Computing GD and GN with every step size
for i =1:length(stepgd)
    % Gradient-Descent
    [Xresult_gdtmp,Xiter_gdtmp,lossval_gdtmp] = gps_estimate(Slsim,y,'X0',X0,'step',stepgd(i),'method','graddescend');
    
    Xresult_gdALL{i}    = Xresult_gdtmp;
    Siter_gdALL{i}      = sqrt(sum((Xiter_gdtmp(1:3,:)-repmat(SrealOut,1,length(Xiter_gdtmp))).^2,1));
    biter_gdALL{i}      = sqrt(sum((Xiter_gdtmp(4,:)-repmat(bsimOut,1,length(Xiter_gdtmp))).^2,1));
    lossval_gdALL{i}    = lossval_gdtmp;
    niters_gd(i)        = length(lossval_gdtmp);
    
    %Gauss-Newton
    [Xresult_gntmp,Xiter_gntmp,lossval_gntmp] = gps_estimate(Slsim,y,'X0',X0,'step',stepgn(i),'method','gauss');
    
    Xresult_gnALL{i}    = Xresult_gntmp;
    Siter_gnALL{i}      = sqrt(sum((Xiter_gntmp(1:3,:)-repmat(SrealOut,1,length(Xiter_gntmp))).^2,1));
    biter_gnALL{i}      = sqrt(sum((Xiter_gntmp(4,:)-repmat(bsimOut,1,length(Xiter_gntmp))).^2,1));
    lossval_gnALL{i}    = lossval_gntmp;
    niters_gn(i)        = length(lossval_gntmp);
end

%% Figure 1
% Not shown in the report.
figure('name','Loss function (l) value per iteration','Units','normalized','Position',[0 0.2944 0.3203 0.5519])

% Line 1
line(1:length(lossval_gd),log(lossval_gd),'Color','r','LineWidth',3);
axis tight;
ax1        = gca; % current axes
set(ax1,'XColor','r','YColor','r','Fontsize',12);
leg1 = legend('Gradient-Descent');
Pos1 = get(leg1, 'Position'); 
set(leg1,'box','off','FontSize',12);
xlabel(ax1,'Iteration','Interpreter','latex');
ylabel('$$\log(\l)$$','Interpreter','latex');

% Line 2
ax1_pos = ax1.Position; % position of first axes
ax2 = axes('Position',ax1_pos,'XAxisLocation','top','YAxisLocation','right','Color','none');
line(1:length(lossval_gn),log(lossval_gn),'Color','b','LineWidth',3);
set(ax2,'XColor','b','YColor','b','Fontsize',12);
axis tight;

leg2 = legend('Gauss-Newton');
set(leg2,'box','off','FontSize',12);
Pos2 = get(leg2, 'Position'); 
set(leg2,'Position',[Pos1(1) Pos1(2)-(Pos2(4)+0.3*Pos2(4)) Pos2(3) Pos2(4)]);
set(get(gcf,'Children'),'Fontsize',17);
grid on;

if flagprint
    set(gcf,'PaperPositionMode','auto');
    print(fullfile(file2savefigs,'Figure1_LossFunctionVsIter_1stepsizeval'),'-depsc','-tiff');
end

%% Figure 2
hfig = figure('Units','normalized','Position',[0.2286 0.2426 0.4490 0.6324]);
% Subplot 1
hsplot1 = subplot(4,2,[1 3 5]);
for i =1:length(stepgd)
    plot(log(lossval_gdALL{i}),'Linewidth',3);
    if i ==1, hold on; end
end
title('Gradient Descent')
ylabel('$$\log(\l)$$','Interpreter','latex');
xlabel('Iteration');
axis tight;
grid on;
hleg1 = legend(num2str(stepgd(1)),num2str(stepgd(2)),num2str(stepgd(3)),num2str(stepgd(4)),num2str(stepgd(5)),num2str(stepgd(6)),num2str(stepgd(7)),num2str(stepgd(8)));
set(hleg1,'box','off');
set(get(gcf,'Children'),'Fontsize',17);

% Subplot 2
hsplot1 = subplot(4,2,[2 4 6]);
for i =1:length(stepgn)
    plot(log(lossval_gnALL{i}),'Linewidth',3);
    if i ==1, hold on; end
end
title('Gauss-Newton')
ylabel('$$\log(\l)$$','Interpreter','latex');
xlabel('Iteration');
axis tight;
grid on;
hleg1 = legend(num2str(stepgn(1)),num2str(stepgn(2)),num2str(stepgn(3)),num2str(stepgn(4)),num2str(stepgn(5)),num2str(stepgn(6)),num2str(stepgn(7)),num2str(stepgn(8)));
set(hleg1,'box','off');
set(get(gcf,'Children'),'Fontsize',17);

% Subplot 3
hsplot1 = subplot(4,2,7);
plot(stepgd,niters_gd,'Linewidth',3)
ylabel('# Iterations');
xlabel('Step size');
hleg3 = legend('Loss-funct val a/convergence');
set(hleg3,'box','off');
axis tight;
grid on;

% Subplot 4
hsplot1 = subplot(4,2,8);
plot(stepgn,niters_gn,'Linewidth',3)
ylabel('# Iterations');
xlabel('Step size');
hleg4 = legend('Loss-funct val a/convergence');
set(hleg4,'box','off');
axis tight;
grid on;

set(get(gcf,'Children'),'Fontsize',17);

if flagprint
    set(gcf,'PaperPositionMode','auto');
    print(fullfile(file2savefigs,'Figure2_LossFunctionVsIter'),'-depsc','-tiff');
end

%% Figure 3
hfig = figure('Units','normalized','Position',[0.2286 0.1611 0.4490 0.7120]);
% Subplot 1
hsplot1 = subplot(2,2,1);
for i =1:length(stepgd)
    plot(log(Siter_gdALL{i}*ER),'Linewidth',3);
    if i ==1, hold on; end
end
title('Gradient Descent')
ylabel('$$\log\|\hat{S}-S\|\qquad\log\boldmath{m}$$','Interpreter','latex');
% xlabel('Iteration');
axis tight;
grid on;
hleg1 = legend(num2str(stepgd(1)),num2str(stepgd(2)),num2str(stepgd(3)),num2str(stepgd(4)),num2str(stepgd(5)),num2str(stepgd(6)),num2str(stepgd(7)),num2str(stepgd(8)));
set(hleg1,'box','off');
set(get(gcf,'Children'),'Fontsize',17);

% Subplot 2
hsplot1 = subplot(2,2,2);
for i =1:length(stepgn)
    plot(log(Siter_gnALL{i}*ER),'Linewidth',3);
    if i ==1, hold on; end
end
title('Gauss-Newton')
ylabel('$$\log\|\hat{S}-S\|\qquad\log\boldmath{m}$$','Interpreter','latex');
% xlabel('Iteration');
axis tight;
grid on;
hleg1 = legend(num2str(stepgn(1)),num2str(stepgn(2)),num2str(stepgn(3)),num2str(stepgn(4)),num2str(stepgn(5)),num2str(stepgn(6)),num2str(stepgn(7)),num2str(stepgn(8)));
set(hleg1,'box','off');
set(get(gcf,'Children'),'Fontsize',17);

% Subplot 3
hsplot1 = subplot(2,2,3);
for i =1:length(stepgd)
    plot(log(biter_gdALL{i}*ER),'Linewidth',3);
    if i ==1, hold on; end
end
ylabel('$$\log\|\hat{b}-b\|\qquad\log\boldmath{m}$$','Interpreter','latex');
xlabel('Iteration');
axis tight;
grid on;
hleg1 = legend(num2str(stepgd(1)),num2str(stepgd(2)),num2str(stepgd(3)),num2str(stepgd(4)),num2str(stepgd(5)),num2str(stepgd(6)),num2str(stepgd(7)),num2str(stepgd(8)));
set(hleg1,'box','off');
set(get(gcf,'Children'),'Fontsize',17);

% Subplot 4
hsplot1 = subplot(2,2,4);
for i =1:length(stepgn)
    plot(log(biter_gnALL{i}*ER),'Linewidth',3);
    if i ==1, hold on; end
end
ylabel('$$\log\|\hat{b}-b\|\qquad\log\boldmath{m}$$','Interpreter','latex');
xlabel('Iteration');
axis tight;
grid on;
hleg1 = legend(num2str(stepgn(1)),num2str(stepgn(2)),num2str(stepgn(3)),num2str(stepgn(4)),num2str(stepgn(5)),num2str(stepgn(6)),num2str(stepgn(7)),num2str(stepgn(8)));
set(hleg1,'box','off');
set(get(gcf,'Children'),'Fontsize',17);

if flagprint
    set(gcf,'PaperPositionMode','auto');
    print(fullfile(file2savefigs,'Figure3_ErrorEstimVsIter'),'-depsc','-tiff');
end