% gps_simulation() - Simulate data for the gps_estimate function
%
% Input:
%
% Optional input:
%   bsim      - Simulated time bias
%   Slsim     - Coordinates of the four satellites. One on each column Dimension[3,4]
%   Sreal     - real value of the receiver. For simulations. Dimension [3,1]
% 
% Outputs:
%   y          - Vector of measures or pseudorange
%   SrealOut   - real value of the receiver. For simulations. Dimension [3,1]
%   SlsimOut   - Coordinates of the four satellites. One on each column Dimension[3,4]
%   bsimOut      - Simulated time bias
%   
% Author: Ramon Martinez-Cancino, UCSD, 2016 (ECE174, Prof. KKD)
function [y, SrealOut, SlsimOut, bsimOut] = gps_simulation(varargin)

% Defaults
Sreal    = [1 0 0]';
bsim     = 2.354788068e-3;
Slsim    = [3.5852   2.0700     0;
            2.9274   2.9274     0;
            2.6612   0          3.1712;
            1.4159   0          3.8904]'; 
try
    options = varargin;
    if ~isempty( varargin ),
        for i = 1:2:numel(options)
            g.(options{i}) = options{i+1};
        end
    else g= []; 
    end;
catch
    disp('gps_simulation() error: calling convention {''key'', value, ... } error'); return;
end;
try g.bsim;      catch, g.bsim   = bsim;       end;
try g.Slsim;     catch, g.Slsim  = Slsim;    end;
try g.Sreal;     catch, g.Sreal  = Sreal;    end;

% Generating noiseles synthetic data
y = nan(4,1);
for i=1:4
    y(i) = norm(g.Slsim(:,i) - g.Sreal)+ g.bsim;
end

SrealOut = g.Sreal;
SlsimOut = g.Slsim;
bsimOut  = g.bsim;
end